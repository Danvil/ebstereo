/* Copyright (C) David Weikersdorfer - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential
 * Written by David Weikersdorfer <davidw@danvil.de>, March 2015
 */

#include "ebstereo.hpp"
#include <boost/program_options.hpp>
#include <dog/dog.hpp>
#include <Edvs/EventStream.hpp>

namespace ebstereo
{

void onStereoEvents(const std::vector<StereoEvent>& stereo_events) {
	// TODO do something useful with the stereo events
	// std::cout << stereo_events.size() << " " << std::flush;
}

int main(int argc, char** argv) {
	// Parse command line parameters.
	std::vector<std::string> p_vuri;///dev/ttyUSB0?baudrate=4000000";
	namespace po = boost::program_options;
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("uris", po::value(&p_vuri)->multitoken(), "URI(s) to event stream(s)")
	;
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);
	if(vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	// Create and initialize stereo algorithm.
	auto stereo_algo = ebstereo::CreateTimefieldPatchStereoAlgorithm();
	stereo_algo->stereo_event_handler() = onStereoEvents;
	// FIXME Set actual camera parameters.
	stereo_algo->camera_model().left.focal_length_px = 64.0f;
	stereo_algo->camera_model().left.center = {64.0f, 64.0f};
	stereo_algo->camera_model().right.focal_length_px = 64.0f;
	stereo_algo->camera_model().right.center = {64.0f, 64.0f};
	stereo_algo->camera_model().baseline = 0.15f;

	// Initialize visualization.
	dog::initialize(dog::CreateOpenGlTarget());
	
	// Open event source.
	auto stream = Edvs::OpenEventStream(p_vuri);
	// Get events and forward them to stereo algorithm.
	while(!stream->eos()) {
		stereo_algo->onRawEvents(stream->read());
	}
	
	// Quit visualization and program.
	dog::finalize();
	return 0;
}

}

int main(int argc, char** argv) {
	return ebstereo::main(argc, argv);
}
