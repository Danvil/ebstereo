/* Copyright (C) David Weikersdorfer - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential
 * Written by David Weikersdorfer <davidw@danvil.de>, March 2015
 */

#include "TimefieldPatchStereo.hpp"

#include <dog/dog.hpp>
#include <slimage/image.hpp>

namespace ebstereo
{

constexpr int MAX_DISPARITY = 32;
constexpr int PATCH_RADIUS = 2;
constexpr int EVENTS_UNTIL_VIS = 200;
constexpr float MS_TO_S = 1.0f / 1000000.0f;
constexpr float VIS_TIME_SCL = 1.0f / 0.5f  * MS_TO_S;
constexpr unsigned RETINA_SIZE = 128;

slimage::Pixel3ub colorizeTime(uint64_t t, uint64_t now) {
	const float dt = static_cast<float>(now - t) * VIS_TIME_SCL;
	// const float p = std::max(0.0f, 1.0f - dt);
	const float p = std::exp(-dt);
	const unsigned char c = static_cast<unsigned char>(255.f * p);
	return {c, c, c};
}

slimage::Image3ub colorizeTime(const EventField<int64_t>& tf) {
	const uint64_t now = tf.maxCoeff();
	slimage::Image3ub img(tf.cols(), tf.rows());
	// Slimage and time fields are both storing data row-major.
	for(size_t i=0; i<img.size(); i++) {
		img[i] = colorizeTime(*(tf.data() + i), now);
	}
	return img;
}

slimage::Pixel3ub colorizeDisparity(float disparity) {
	if(disparity < 0) {
		return {0, 0, 0};
	}
	const float p = disparity / static_cast<float>(MAX_DISPARITY);
	const unsigned char c = static_cast<unsigned char>(255.f * p);
	return slimage::Pixel3ub{c, 0, static_cast<unsigned char>(255) - c};
}

slimage::Image3ub colorizeDisparity(const EventField<float>& matrix) {
	slimage::Image3ub img(matrix.cols(), matrix.rows());
	// Slimage and time fields are both storing data row-major.
	for(size_t i=0; i<img.size(); i++) {
		img[i] = colorizeDisparity(*(matrix.data() + i));
	}
	return img;
}

slimage::Image3ub multImages(const slimage::Image3ub& a, const slimage::Image3ub& b) {
	slimage::Image3ub img(a.width(), a.height());
	for(size_t i=0; i<a.size(); i++) {
		const auto& pa = a[i];
		const auto& pb = b[i];
		unsigned cr = static_cast<unsigned>(pa[0]) * static_cast<unsigned>(pb[0]);
		unsigned cg = static_cast<unsigned>(pa[1]) * static_cast<unsigned>(pb[1]);
		unsigned cb = static_cast<unsigned>(pa[2]) * static_cast<unsigned>(pb[2]);
		img[i] = {
			static_cast<unsigned char>(cr / 256),
			static_cast<unsigned char>(cg / 256),
			static_cast<unsigned char>(cb / 256)
		};
	}
	return img;
}

StereoEventAlgorithm* CreateTimefieldPatchStereoAlgorithm() {
	return new TimefieldPatchStereo();
}

TimefieldPatchStereo::TimefieldPatchStereo() {
	// FIXME correct initialization using actual camera resolution
	for(unsigned i=0; i<NUM_CAMERAS; i++) {
		auto& tf = time_fields_[i];
		tf = EventField<int64_t>(RETINA_SIZE, RETINA_SIZE);
		std::fill(tf.data(), tf.data() + tf.size(), 0);
	}
	disparity_field_ = Eigen::MatrixXf::Constant(RETINA_SIZE, RETINA_SIZE, -1.0f);
}

template<typename M1, typename M2>
float PatchError(const M1& a, const M2& b) {
	dog::enforce(a.rows() == b.rows(), "Number of rows do not match!");
	dog::enforce(a.cols() == b.cols(), "Number of cols do not match!");
	// The element type of a and b has to be signed, otherwise the difference will produce unexpected results!
	return ((a - b).array().template cast<float>() * MS_TO_S).pow(2.0f).sum();
}

/** @param Computes disparity by moving a patch horizontal over a target region and finding best fit */
template<typename M1, typename M2>
float ComputeDisparity(const M1& patch, const M2& patch_row) {
	const unsigned rows = patch.rows();
	const unsigned cols = patch.cols();
	dog::enforce(patch_row.rows() == rows, "Number of rows do not match!");
	dog::enforce(patch_row.cols() >= cols, "Number of cols do not match!");
	const unsigned n = patch_row.cols() - cols;
	float best_error = std::numeric_limits<float>::max();
	int best_index = -1;
	for(unsigned i=0; i<n; i++) {
		float error = PatchError(patch, patch_row.block(0, i, rows, cols));
		if(error < best_error) {
			best_error = error;
			best_index = i;
		}
	}
	// TODO subpixel accuracy
	return static_cast<float>(best_index);
}

/** @param Computes disparity at given pixel (searches in second field in positive x direction) */
template<typename M1, typename M2>
float ComputeDisparity(const M1& tf1, const M2& tf2, int x, int y) {
	const unsigned rows = tf1.rows();
	const unsigned cols = tf1.cols();
	dog::enforce(tf2.rows() == rows, "Number of rows do not match!");
	dog::enforce(tf2.cols() == cols, "Number of cols do not match!");
	if(x < PATCH_RADIUS || cols <= x + PATCH_RADIUS + MAX_DISPARITY || y < PATCH_RADIUS || rows < y + PATCH_RADIUS) {
		return -1.0f;
	}
	constexpr int s = 2*PATCH_RADIUS + 1;
	return ComputeDisparity(
		tf1.block(y - PATCH_RADIUS, x - PATCH_RADIUS, s, s),
		tf2.block(y - PATCH_RADIUS, x - PATCH_RADIUS, s, s + MAX_DISPARITY)
	);
}

void TimefieldPatchStereo::onRawEvents(const std::vector<Edvs::Event>& events) {
	if(events.empty()) {
		return;
	}
	std::vector<StereoEvent> stereo_events;
	for(const auto& event : events) {
		// TODO Do this rotation somewhere else
		const int ex = event.x; // 127 - event.y;
		const int ey = 127 - event.y; // event.x;
		const int eid = event.id;
		time_fields_[eid](ey, ex) = event.t;
		if(eid == RIGHT_ID) {
			const float disparity = ComputeDisparity(time_fields_[RIGHT_ID], time_fields_[LEFT_ID], ex, ey);
			disparity_field_(ey, ex) = disparity;
			stereo_events.push_back({event, disparity, camera_model().backproject(ex, ey, disparity)});
		}
		num_since_vis_[eid]++;
	}
	onStereoEvents(stereo_events);
	// visualization
	const uint64_t now = events.back().t;
	for(unsigned i=0; i<NUM_CAMERAS; i++) {
		if(num_since_vis_[i] > EVENTS_UNTIL_VIS) {
			num_since_vis_[i] = 0;
		}
		std::string tag = "event time field ";
		tag += std::string(1, '0' + i);
		dog::feed({tag, now}, [this,i]() { return colorizeTime(time_fields_[i]); });
	}
	dog::feed({"event disparity", now}, [this]() {
		slimage::Image3ub img_time = colorizeTime(time_fields_[RIGHT_ID]);
		slimage::Image3ub img_disp = colorizeDisparity(disparity_field_);
		return multImages(img_time, img_disp);
	});
}

}
