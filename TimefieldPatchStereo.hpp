/* Copyright (C) David Weikersdorfer - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential
 * Written by David Weikersdorfer <davidw@danvil.de>, March 2015
 */

#pragma once

#include "ebstereo.hpp"

#include <Eigen/Core>

namespace ebstereo
{

/** @param An event field storing data for each retina pixel */
template<typename T>
using EventField = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

/** @param An event-based stereo algorithm using basic patch matching */
class TimefieldPatchStereo : public StereoEventAlgorithm
{
public:
	TimefieldPatchStereo();

	void onRawEvents(const std::vector<Edvs::Event>& events) override;

private:
	static constexpr unsigned NUM_CAMERAS = 2;
	static_assert(NUM_CAMERAS == 2, "Currently only two camera are supported!");
	static constexpr int LEFT_ID = 0;
	static constexpr int RIGHT_ID = 1;

	EventField<int64_t> time_fields_[NUM_CAMERAS];
	EventField<float> disparity_field_;
	unsigned num_since_vis_[2] = {0};
};

}
