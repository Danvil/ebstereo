/* Copyright (C) David Weikersdorfer - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential
 * Written by David Weikersdorfer <davidw@danvil.de>, March 2015
 */

#pragma once

#include <Eigen/Core>

#include <dog/assert.hpp>
#include <Edvs/Event.hpp>

namespace ebstereo
{

/** @brief A stereo event consisting of raw event, disparity and 3D point */
struct StereoEvent
{
	Edvs::Event original_event;
	float disparity;
	Eigen::Vector3f point;
};

/** @brief A camera pinhole model */
struct PinholeCameraModel
{
	float focal_length_px;
	Eigen::Vector2f center;

	Eigen::Vector2f project(const Eigen::Vector3f& point) const {
		const float scl = focal_length_px / point.z();
		return center + scl * point.block<2,1>(0,0);
	}

	Eigen::Vector3f backproject(int x, int y, float depth) const {
		const Eigen::Vector2f uv{static_cast<float>(x), static_cast<float>(y)};
		const float scl = depth / focal_length_px;
		const Eigen::Vector2f xy = scl * (uv - center);
		return { xy.x(), xy.y(), depth };
	}
};

/** @brief A stereo camera pinhole model */
struct StereoPinholeCameraModel {
	PinholeCameraModel left, right;
	float baseline = 0.1f; // FIXME

	float disparityToDepth(float disparity) const {
		// TODO do real triangulation
		return baseline * left.focal_length_px / disparity;
	}

	Eigen::Vector3f backproject(int x, int y, float depth) const {
		return right.backproject(x, y, disparityToDepth(depth));
	}
};

/** @brief Interface for an event-based stereo algorithm
 * Raw events from the sensor are given via the function OnRawEvents.
 * Stereo events are output via an stereo event handler. The algorithm is permitted
 * to have a certain delay for stereo computation to increase quality. 
 */
class StereoEventAlgorithm
{
public:
	/** @brief Forwards raw events from the device to the stereo algorithm */
	virtual void onRawEvents(const std::vector<Edvs::Event>& events) = 0;

public:
	using StereoEventHandler = std::function<void(const std::vector<StereoEvent>&)>;
	
	const StereoEventHandler& stereo_event_handler() const { return stereo_event_handler_; }
	StereoEventHandler& stereo_event_handler() { return stereo_event_handler_; }

	const StereoPinholeCameraModel& camera_model() const { return camera_model_; }
	StereoPinholeCameraModel& camera_model() { return camera_model_; }

protected:
	/** @brief Used by the algorithm to output stereo event results */
	void onStereoEvents(const std::vector<StereoEvent>& stereo_events) {
		if(stereo_event_handler_ && !stereo_events.empty()) {
			stereo_event_handler_(stereo_events);
		}
	}

private:
	StereoPinholeCameraModel camera_model_;
	StereoEventHandler stereo_event_handler_;
};

/** @brief Creates a basic timefield patch convolution stereo algorithm */
StereoEventAlgorithm* CreateTimefieldPatchStereoAlgorithm();

}
